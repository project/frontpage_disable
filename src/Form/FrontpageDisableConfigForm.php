<?php

namespace Drupal\frontpage_disable\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FrontpageDisableConfigForm.
 *
 * @package Drupal\frontpage_disable\Form
 */
class FrontpageDisableConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames():array {
    return ['frontpage_disable.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId():string {
    return 'frontpage_disable_settings';
  }

  /**
   * Get config name.
   *
   * @return string
   *   Config name.
   */
  protected function getConfigName():string {
    return $this->getEditableConfigNames()[0];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state):array {
    $config = $this->config($this->getConfigName());
    $form['disabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Disable frontpage'),
      '#default_value' => $config->get('disabled') ?? FALSE,
    ];
    $form['redirect_to'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Redirect to'),
      '#default_value' => $config->get('redirect_to') ?? '',
      '#states'        => [
        'required' => [':input[name="disabled"]' => ['checked' => 'checked']],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('disabled')
      && $form_state->getValue('redirect_to') == ''
    ) {
      $form_state->setErrorByName(
        'redirect_to',
        'Redirect To is required when frontpage is disabled.'
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getConfigName());
    $config->set('disabled', $form_state->getValue('disabled'));
    $config->set('redirect_to', Xss::filterAdmin($form_state->getValue('redirect_to')));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
