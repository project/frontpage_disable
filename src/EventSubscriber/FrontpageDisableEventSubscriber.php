<?php

namespace Drupal\frontpage_disable\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class FrontpageDisableEventSubscriber.
 *
 * @package Drupal\frontpage_disable\EventSubscriber
 */
class FrontpageDisableEventSubscriber implements EventSubscriberInterface, ContainerInjectionInterface {

  /**
   * Config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;
  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * FrontpageDisableEventSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path matcher.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    PathMatcherInterface $path_matcher
  ) {
    $this->config      = $config_factory->get('frontpage_disable.settings');
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):FrontpageDisableEventSubscriber {
    return new static(
      $container->get('config.factory'),
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents():array {
    $events[KernelEvents::REQUEST] = ['processDisablingFrontpage', -100];
    return $events;
  }

  /**
   * Process disabling frontpage and redirecting to another page.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   GetResponseEvent object.
   */
  public function processDisablingFrontpage(GetResponseEvent $event) {
    // Ensure current page is the frontpage and it is disabled.
    if ($this->pathMatcher->isFrontPage() && $this->isDisabled()) {
      // Build redirect object.
      $redirect = $this->buildRedirect();
      // Perform redirect.
      if ($redirect instanceof RedirectResponse) {
        $event->setResponse($redirect);
      }
    }
  }

  /**
   * Check if frontpage is disabled.
   *
   * @return bool
   *   TRUE - if disabled, FALSE otherwise.
   */
  protected function isDisabled():bool {
    return ((boolean) $this->config->get('disabled')) === TRUE;
  }

  /**
   * Build redirect response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Redirect response object.
   */
  protected function buildRedirect() {
    $path = $this->config->get('redirect_to');
    if (!empty($path)) {
      return stripos($path, 'http') === 0
        // External URL.
        ? new TrustedRedirectResponse($path, 301)
        // Internal URL.
        : new LocalRedirectResponse($path, 301);
    }
    return NULL;
  }

}
