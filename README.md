Frontpage Disable
=================

INTRODUCTION
------------

Disables access to frontpage and adds
 a permanent redirect to the page specified by admin

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
 https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* Go to the admin/config/system/frontpage_disable
* Switch ON checkbox
* Specify URL to redirect from the frontpage
