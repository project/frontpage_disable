<?php

namespace Drupal\Tests\frontpage_disable\Kernel;

use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FrontpageDisableRedirectTest.
 *
 * @group frontpage_disable
 *
 * @package Drupal\Tests\frontpage_disable\Kernel
 */
class FrontpageDisableRedirectTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'frontpage_disable',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installConfig(['frontpage_disable', 'user', 'system']);
  }

  /**
   * Perform HTTP request to the frontpage.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   *
   * @throws \Exception
   */
  protected function requestToFrontpage() {
    /** @var \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel */
    $http_kernel = $this->container->get('http_kernel');
    $request = Request::create(Url::fromRoute('<front>')->toString());
    return $http_kernel->handle($request);
  }

  /**
   * Test redirect from homepage to "/user/login".
   */
  public function testRedirect() {
    // Configure redirect from frontpage to "/user/login".
    $conf = $this->config('frontpage_disable.settings');
    $conf->set('disabled', TRUE);
    $conf->set('redirect_to', '/user/login');
    $conf->save();

    $response = $this->requestToFrontpage();

    $this->assertNotInstanceOf(HtmlResponse::class, $response, 'The response is not the HtmlResponse.');
    $this->assertInstanceOf(RedirectResponse::class, $response, 'The response is a redirect.');
    $this->assertTrue(method_exists($response, 'getTargetUrl'), 'The response has getTargetUrl method.');
    $this->assertEquals('/user/login', $response->getTargetUrl(), 'The redirect target is the "/user/login".');
  }

  /**
   * Test external redirect from frontpage to "https://drupal.org".
   */
  public function testExternalRedirect() {
    static $redirect_to = 'https://drupal.org';
    // Configure redirect from frontpage to "https://drupal.org".
    $conf = $this->config('frontpage_disable.settings');
    $conf->set('disabled', TRUE);
    $conf->set('redirect_to', $redirect_to);
    $conf->save();

    $response = $this->requestToFrontpage();

    $this->assertNotInstanceOf(HtmlResponse::class, $response, 'The response is not the HtmlResponse.');
    $this->assertNotInstanceOf(LocalRedirectResponse::class, $response, 'The response is not the LocalRedirectResponse.');
    $this->assertInstanceOf(TrustedRedirectResponse::class, $response, 'The response is TrustedRedirectResponse.');
    $this->assertTrue(method_exists($response, 'getTargetUrl'), 'The response has getTargetUrl method.');
    $this->assertEquals($redirect_to, $response->getTargetUrl(), 'The redirect target is the "https://drupal.org".');
  }

}
