<?php

namespace Drupal\Tests\frontpage_disable\Kernel;

use Drupal\frontpage_disable\EventSubscriber\FrontpageDisableEventSubscriber;
use Drupal\frontpage_disable\Form\FrontpageDisableConfigForm;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\Routing\Route;

/**
 * Class FrontpageDisableComponentsTest.
 *
 * @group frontpage_disable
 *
 * @package Drupal\Tests\frontpage_disable\Kernel
 */
class FrontpageDisableComponentsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'frontpage_disable',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installConfig(['frontpage_disable', 'user', 'system']);
  }

  /**
   * Test that all module's components are exist.
   */
  public function testModuleComponents() {
    $route = $this->container->get('router.route_provider')
      ->getRouteByName('frontpage_disable.config');
    $links = $this->container->get('plugin.manager.menu.link')
      ->loadLinksByRoute('frontpage_disable.config');
    $this->assertTrue(class_exists(FrontpageDisableEventSubscriber::class), 'Event subscriber class exists');
    $this->assertTrue(class_exists(FrontpageDisableConfigForm::class), 'Config form class exists');
    $this->assertInstanceOf(Route::class, $route, 'Config form route exists');
    $this->assertNotEmpty($links, 'Menu link is registered');
    $this->assertTrue($this->container->has('frontpage_disable.redirect_subscriber'), 'frontpage_disable.redirect_subscriber is registered as a service');
  }

}
