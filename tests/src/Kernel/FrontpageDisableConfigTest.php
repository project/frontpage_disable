<?php

namespace Drupal\Tests\frontpage_disable\Kernel;

use Drupal\Core\Config\Config;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class FrontpageDisableConfigTest.
 *
 * @group frontpage_disable
 *
 * @package Drupal\Tests\frontpage_disable\Kernel
 */
class FrontpageDisableConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'frontpage_disable',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installConfig(['frontpage_disable', 'user', 'system']);
  }

  /**
   * Test configs.
   */
  public function testConfigs() {
    $conf = $this->config('frontpage_disable.settings');
    $this->assertInstanceOf(Config::class, $conf, 'Config object exists');
    $this->assertFalse($conf->get('disabled'), 'Frontpage disabling is switched OFF');
    $this->assertEquals($conf->get('redirect_to'), '/user/login', 'Redirect URL defaults to "/user/login"');
    $conf->set('disabled', TRUE)->save();
    $this->assertTrue($conf->get('disabled'), 'Frontpage disabling is switched ON');
  }

}
