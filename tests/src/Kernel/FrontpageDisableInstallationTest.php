<?php

namespace Drupal\Tests\frontpage_disable\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Class FrontpageDisableInstallationTest.
 *
 * @group frontpage_disable
 *
 * @package Drupal\Tests\frontpage_disable\Kernel
 */
class FrontpageDisableInstallationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'frontpage_disable',
  ];

  /**
   * Test that module has been installed along with it's dependencies.
   */
  public function testModuleInstallation() {
    $module_handler = $this->container->get('module_handler');
    $this->assertTrue($module_handler->moduleExists('frontpage_disable'));
    $this->assertTrue($module_handler->moduleExists('system'));
    $this->assertTrue($module_handler->moduleExists('user'));
  }

}
